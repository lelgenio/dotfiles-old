#!/bin/sh
# set -e
# set -x

comment(){
    local file="$1";shift
    sudo perl -i -pe "s/^#* */# / if /$@/" $file
}

uncomment(){
    local file="$1";shift
    sudo perl -i -pe "s/^#* *// if /$@/" $file
}

gui_off (){
    # sudo chsh -s /usr/bin/zsh $(whoami)

    rm ~/.config/mimeapps.list
    cd ~/.dotfiles
    stow mime

    uncomment /etc/mkinitcpio.conf "HOOKS.* (?!plymouth)"
    comment   /etc/mkinitcpio.conf "HOOKS.* plymouth"
    yay -Rcs plymouth --noconfirm || true
    sudo mkinitcpio -P

    comment /boot/loader/entries/arch.conf "options quiet"

    sudo systemctl disable NetworkManager
    sudo systemctl enable  netctl-auto@wlp1s0
    sudo systemctl enable  netctl-ifplugd@enp2s0
    sudo systemctl enable  netctl

    sudo systemctl disable display-manager.service

    sudo systemctl enable ly

}

gui_on (){
    # sudo chsh -s /usr/bin/fish $(whoami)

    rm -f ~/.config/mimeapps.list
    yay -S --noconfirm --needed gnome-defaults-list
    cp /usr/share/applications/gnome-mimeapps.list ~/.config/mimeapps.list

    kvantummanager --set Materia
    gsettings set org.gnome.desktop.interface icon-theme '{{@@ icon_theme @@}}'

    comment     /etc/mkinitcpio.conf "HOOKS.* (!?plymouth)"
    uncomment   /etc/mkinitcpio.conf "HOOKS.* plymouth"
    yay -S plymouth plymouth-theme-arch-glow --noconfirm --needed
    sudo plymouth-set-default-theme arch-glow
    sudo mkinitcpio -P

    uncomment /boot/loader/entries/arch.conf "options quiet"

    sudo systemctl disable  netctl-auto@wlp1s0
    sudo systemctl disable  netctl-ifplugd@enp2s0
    sudo systemctl disable  netctl
    sudo systemctl enable  NetworkManager

    sudo systemctl disable display-manager.service

    sudo systemctl enable gdm-plymouth
}

restart_gdm(){
    if systemctl -q is-active gdm-plymouth
    then
        sudo systemctl restart gdm-plymouth
    fi

    if systemctl -q is-active gdm
    then
        sudo systemctl restart gdm
    fi
}

x11_on(){
    uncomment   /etc/gdm/custom.conf "Wayland"
    perl -i -pe 's/false/true/ if /layers.acceleration.force-enable/' ~/.mozilla/firefox/*default-release*/prefs*js
    restart_gdm
}

x11_off(){
    comment     /etc/gdm/custom.conf "Wayland"
    perl -i -pe 's/true/false/ if /layers.acceleration.force-enable/' ~/.mozilla/firefox/*default-release*/prefs*js
    restart_gdm
}

if [ "$1" = "on" ];then
    gui_on
elif [ "$1" = "off" ];then
    gui_off
elif [ "$1" = "x11" ];then
    if [ "$2" = "on" ];then
        x11_on
    elif [ "$2" = "off" ];then
        x11_off
    fi
else
    echo "Usage: "
    echo "  $0      <on|off>"
    echo "  $0 x11  <on|off>"
fi
